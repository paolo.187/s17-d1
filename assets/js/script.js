

// const array1 = ['eat', 'sleep'];
// console.log(array1);


let array2 = new Array(1,2,3);

console.log(array2)


let travelList = ['NY','Japan','Spain','Australia','California','Miami','Boracay'];

console.log(travelList[0]);
console.log(travelList);
console.log(travelList[travelList.length-1]);

for(i=0; i <= travelList.length-1; i++){
	console.log(travelList[i])
}


let dailyActivities=[
	'eat',
	'work',
	'pray',
	'play'
];

dailyActivities.push('exercise');// adds element at the end

console.log(dailyActivities);

dailyActivities.unshift('sleep');// adds element in the beginning

console.log(dailyActivities);

dailyActivities[2]= 'sing'; //swiches element

console.log(dailyActivities)

dailyActivities[6]= 'dance'; // since none to switch, auto add

console.log(dailyActivities)


travelList[3] = "Egypt"

console.log(travelList);



travelList[0] = 'Pasig'
travelList[travelList.length-1] = 'LSGH'

console.log(travelList)
console.log(travelList[0], travelList[travelList.length-1])


travelList.pop(); //removes last element in array

console.log(travelList);
console.log(travelList.pop());

let removedItem = travelList.pop();
console.log(travelList);
console.log(removedItem);

let removedItemShift = travelList.shift();
console.log(travelList)
console.log(removedItemShift)

	let array1 = ['Juan', 'Pedro', 'Jose', 'Andres'];
	// // without method
	// array1[array.length] = 'Francisco';
	// console.log(array1);

	// // .push() - allows us to add an element at the end of the array
	// array1.push('Andres');
	// console.log(array1);

	// // .unshift() - allows us to add an element at the beginning of the array
	// array1.unshift('Simon');
	// console.log(array1);

	// // .pop() - allows us to delete or remove the last item/element at the end of the array
	// array1.pop();
	// console.log(array1);
	// // .pop() is also able to return the item we removed.
	// console.log(array1.pop());
	// console.log(array1);

	// let removedItem = array1.pop();
	// console.log(array1);
	// console.log(removedItem);

	// // .shift() return the item we removed
	// let removedItemShift = array1.shift();
	// console.log(array1);
	// console.log(removedItemShift);

// mini activity

console.log(array1.shift());
console.log(array1);
array1.pop();
console.log(array1);
array1.unshift('George');
console.log(array1);
array1.push('Michael');
console.log(array1);


let numArray = [3,2,1,6,7,9];

numArray.sort();
console.log(numArray);

let numArray2 = [32,400,450,2,9,5,90,50,];

numArray2.sort();
console.log(numArray2);

numArray2.sort((a,b)=>a-b)
console.log(numArray2)

let arrayStr = ['Marie','Zen','Jaime','Elaine'];
arrayStr.sort(function(a,b){
	return b-a
})
arrayStr.sort();
console.log(arrayStr);

arrayStr.sort().reverse();
console.log(arrayStr);

let beatles = ['George','John','Paul', 'Ringo'];

let lakers = ['Lebron','Davis','Westbrook','Kobe','Shaq'];
//splice() -aalws ro remove and add elements from a given index. syntax: array.splice(startingIndex, number of items to be deleted, elements to add)

lakers.splice(0,0,'Caruso')
console.log(lakers)

lakers.splice(0,1)
console.log(lakers)

lakers.splice(0,3)
console.log(lakers)

lakers.splice(1,1)
console.log(lakers)

lakers.splice(1,0, 'Gasol', 'Fischer')
console.log(lakers);


//slice()-allows us to get a portion of a new array with the items selected from the original. syntax : slice(startindex, end index)

let computerBrands = ['IBM','HP','Apple','MSI'];

computerBrands.splice(2,2,'Compaq','Toshiba','Acer')
console.log(computerBrands)

let newBrands = computerBrands.slice(1,3);

console.log(computerBrands);
console.log(newBrands);

let fonts = ['TNR','Comic Sans', 'Impact', 'monoptype', 'arial', 'arial black']





console.log(fonts)
let newFontSet = fonts.slice(1,4)
console.log(newFontSet)
console.log(fonts)


let videoGame = ['PS4','PS5','Switch','Xbox','Xbox1']




let microsoft = videoGame.slice(3);

let nintendo = videoGame.slice(2,4);

console.log(microsoft)
console.log(nintendo)

// .toString() - convert the array into a single value as a string but each item will be separated by a comma
// syntax: array.toString()

let sentence = ['I', 'like', 'JavaScript', '.', 'It', 'is', 'fun', '.'];
let sentenceString = sentence.toString();
console.log(sentence);
console.log(sentenceString);

//.join() - converts the array into a single value as a string byt separator can be specified
//syntax: array.join(separator)
let sentence2 = ['My', 'favorite', 'fastfood', 'is', 'Army Navy'];
let sentenceString2 = sentence2.join(' ');
console.log(sentenceString2);
let sentenceString3 = sentence2.join("/");
console.log(sentenceString3);


   let n = [1,2,3,4,10,11];
    console.log(n.length)



//     let ar= 0;
// function simpleArraySum(ar) {
    
//         for(let i=0; i<= n.length-1; i++){
//             ar += n[i];
//         }
//     console.log(ar)
// }
// simpleArraySum(ar)


let name1 = ['M','a','r','t','i','n'];
	console.log(name1);
let nameString1= name1.toString();
	console.log(nameString1);
let nameString1J= name1.join('');
	console.log(nameString1J);


let name2 = ['M','i','g','u','e','l'];
	console.log(name2);
let nameString2 = name2.toString();
	console.log(nameString2);
	let nameString2J= name2.join('');
	console.log(nameString2J);


let taskFriday = ['drink HTML', 'eat Javascript'];
let taskSaturday = ['inhale CSS', 'breath Bootstrap'];
let taskSunday = ['Get git', 'be Node'];

let weekendTask = taskFriday.concat(taskSaturday,taskSunday);

console.log(weekendTask);

// accessors

// let batch131 = [
// 'Paolo',
// 'Jamir',
// 'Jed',
// 'Ronel',
// 'Rom',
// 'Jayson'
// ];

// console.log(batch131.indexOf('Jed'))
// console.log(batch131.indexOf('Paolo'))


// console.log(batch131.lastIndexOf('Jamir'))
// console.log(batch131.lastIndexOf('Jayson'))

let carBrands = [
			'BMW',
			'Dodge',
			'Maserati',
			'Porsche',
			'Chevrolet',
			'Ferrari',
			'GMC',
			'Porsche',
			'Mitsubhisi',
			'Toyota',
			'Volkswagen',
			'BMW'
		];



function carPrint1 (car1){
	console.log(carBrands.indexOf(car1));
}

carPrint1('GMC');

function carPrint2 (car2){
	console.log(carBrands.lastIndexOf(car2));
}

carPrint2('Volkswagen');

 
// .concat() - it combines 2 or more arrays without affecting the original
// syntax: array.concat(array1, array2)

let tasksFriday = ['drink HTML', 'eat JavaScript'];
let tasksSaturday = ['inhale CSS', 'breath BootStrap'];
let tasksSunday = ['Get Git', 'Be Node'];

let weekendTasks = tasksFriday.concat(tasksSaturday,tasksSunday);
console.log(weekendTasks);

// Accessors
	// Methods that allow us to access our array.
	// indexOf()
	// 	- finds the index of the given element/item when it is first founc from the left.

let batch131 = [
		'Paolo',
		'Jamir',
		'Jed',
		'Ronel',
		'Rom',
		'Jayson'
];

console.log(batch131.indexOf('Jed'));//2
console.log(batch131.indexOf('Rom'));//4

//lastIndexOf()
	// - finds the index of the given element/item when it is last found from the right
console.log(batch131.lastIndexOf('Jamir'));//1
console.log(batch131.lastIndexOf('Jayson'));//5

console.log(batch131.lastIndexOf('Kim'));

/*
	Mini-Activity
	Given a set of brands with some entries repeated:
		Create a function which can display the index of the brand that was input the first time it was found in the array.

		Create a function which can display the index of the brand that was input the last time it was found in the array.
*/
		// let carBrands = [
		// 	'BMW',
		// 	'Dodge',
		// 	'Maserati',
		// 	'Porsche',
		// 	'Chevrolet',
		// 	'Ferrari',
		// 	'GMC',
		// 	'Porsche',
		// 	'Mitsubhisi',
		// 	'Toyota',
		// 	'Volkswagen',
		// 	'BMW'
		// ];

function indexFinder(brand){
	console.log(carBrands.indexOf(brand));
};

function lastIndexFinder(brand){
	console.log(carBrands.lastIndexOf(brand));
};

indexFinder('BMW');
lastIndexFinder('Porsche');

// Iterator Methods
	// These methods iterate over the itemns in an array much like loop
	// However, with our iterator methods there also that allows to not only iterate over items but also additional instruction.

let avengers = [
		'Hulk',
		'Black Widow',
		'Hawkeye',
		'Spider-man',
		'Iron Man',
		'Captain America'
];

// forEach() 
	//similar to for loop but is used on arrays. It will allow us to iterate over each time in an array and even add instruction per iteration
//Anonymous function within forEach will be receive each and evey item in an array

avengers.forEach(function(avenger){
	console.log(avenger);
});


let marvelHereos = [
		'Moon Knight',
		'Jessica Jones',
		'Deadpool',
		'Cyclops'
];

marvelHereos.forEach(function(hero){
	// iterate over all the items in Marvel Heroes array and let them join the avengers
	if(hero !== 'Cyclops' && hero !== 'Deadpool'){
		// Add an if-else wherein Cyclops and Deadpool is not allow to join
		avengers.push(hero);
	}
});
console.log(avengers);

// map() - similar to forEach however it returns new array

let number = [25, 50, 30, 10, 5];

let mappedNumbers = number.map(function(number){
	console.log(number);
	return number * 5
});
console.log(mappedNumbers);

// every()
	// iterates ovel all the items and checks if all the elements passess a given condition

let allMemberAge = [25, 30, 15, 20, 26];

let checkAllAdult = allMemberAge.every(function(age){
	console.log(age);
	return age >= 18
});

console.log(checkAllAdult);
// some()
	// iterate over all the items check if even at least one of items in the array passes the condition. Same as every(), it will return boolean
let examScores = [75, 80, 74, 71];
let checkForPassing = examScores.some(function(score){
	console.log(score);
	return score >= 80;
});
console.log(checkForPassing);

// filter() - creates a new array that contains elements which passed a given condition

let numbersArr2 = [500, 12, 120, 60, 6, 30];

let divisibleBy5 = numbersArr2.filter(function(number){
	return number % 5 === 0;
});
console.log(divisibleBy5);

// find()- iterate over all items in our array but only returns the item that will statisfy the given condition

let registeredUsernames = ['pedro101','mikeyTheKing2000', 'superPhoenix', 'sheWhoCode'];

let foundUser = registeredUsernames.find(function(username){
	console.log(username);
	return (username === "mikeyTheKing2000");
})
console.log(foundUser);


// .includes()- returns a boolean if it finds a matching item in the array.//case sensitive.

let registeredEmails = [
	'johnnyPhoenix1991@gmail.com',
	'michaelKing@gmail.com',
	'pedro_himself@yahoo.com',
	'sheJonesSmith@gmail.com'
];

let doesEmailExist = registeredEmails.includes('michaelKing@gmail.com');
console.log(doesEmailExist);


//miniactivity



let findUser = registeredUsernames.find(function(username){
	console.log(username);
	return (username === "pedro101");
})
console.log(findUser);



function findEmail (email){
	let emailCheck = registeredEmails.includes(email);
	if (emailCheck === true){
		alert(`${email} already exists` );
	}else {
		alert(`${email} available, proceed to Registration`)
	}

}

findEmail('johnnyPhoenix1991@gmail.com')

